const app = require('./app');
const fs = require('fs');

const http = require('http');
// const https = require('https');

// https://blog.cloudboost.io/everything-about-creating-an-https-server-using-node-js-2fc5c48a8d4e
const key = fs.readFileSync('encryption/private.key');
const cert = fs.readFileSync( 'encryption/betterfinalyearproject.crt' );
// const ca = fs.readFileSync( 'encryption/intermediate.crt' );
const options = {
    key: key,
    cert: cert
    // , ca: ca
  };

//const port = process.env.PORT || 443;
const port = process.env.PORT || 3000;

const server = http.createServer(app).listen(port);
// const server = https.createServer(options, app).listen(port);

console.log("listening on port: ", port);