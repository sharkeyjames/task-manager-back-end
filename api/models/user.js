const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: String,
    passwordHash: String,
    manager: Boolean
});

module.exports = mongoose.model('User', userSchema);