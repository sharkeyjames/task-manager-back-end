const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const session = require('express-session')

const userRoutes = require("./api/routes/users");
const projectRoutes = require("./api/routes/projects");
const filterRoutes = require("./api/routes/filters");

const secret = process.env.SECRET || "***This is my super secret key***";

mongoose.Promise = global.Promise;
mongoose.connect(
  "mongodb://sharkeyj07:" +
  process.env.MONGO_ATLAS_PW +
  "@test1-shard-00-00-fn62f.mongodb.net:27017,test1-shard-00-01-fn62f.mongodb.net:27017,test1-shard-00-02-fn62f.mongodb.net:27017/test?ssl=true&replicaSet=Test1-shard-0&authSource=admin",
  {
    useMongoClient: true
  }
);

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({secret:secret, resave:false, saveUninitialized:true}))

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

// Routes which should handle requests
app.use("/users", userRoutes);
app.use("/projects", projectRoutes);
app.use("/filters", filterRoutes);

app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
