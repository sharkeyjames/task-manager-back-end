const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Filter = require("../models/filter");

//Returns all Filters
//Testing Method
router.get("/", (req, res, next) => {
  Filter.find()
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Returns all filters for a given user and associated poject
router.get("/:username/:projectId", (req, res, next) => {
  const username = req.params.username;
  const projectId = req.params.projectId;
  Filter.find({ user: username, project_id: projectId })
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Overwrites all filters for a given user and associated poject with given filters
//Should check that the user is the logged in user
router.post("/save", (req, res, next) => {
  const filters = req.body.fiters;
  var toAdd = Filter[filters.length];
  var previous = Filter[filters.length];
  filters.forEach(function (filterSet, i) {
    var filter = new Filter({
      _id: new mongoose.Types.ObjectId(),
      project_id: filterSet.project_id,
      user: filterSet.user,
      tabNumber: filterSet.tabNumber,
      sprint: filterSet.sprint,
      priorities: filterSet.priorities,
      statuses: filterSet.statuses,
      assignedTo: filterSet.assignedTo
    });
    toAdd[i] = filter;
  });
  if (toAdd.length !== 0) {
    Filter.deleteMany({ user: username, project_id: projectId })
      .exec()
      .then(deleted => {
        Filter.insertMany(toAdd)
          .exec()
          .then(docs => {
            console.log(docs);
            res.status(200).json(docs);
          })
          .catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  }
  console.log("No filters provided");
  res.status(200).json(toAdd);
});

//Deletes all filters for a given user and associated poject with given filters
router.delete("/", (req, res, next) => {
  const username = req.body.username;
  const project_id = req.body.project_id;
  Filter.deleteMany({ user: username, project_id: projectId })
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
