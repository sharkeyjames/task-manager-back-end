const mongoose = require('mongoose');

const projectSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: String,
    admins: [String],
    users: [String],
    tickets: [{ID: Number,
               title: String,
               priority: Number, 
               sprint: Number, 
               hours: [{user: String,
                        value: Number,
                        dateAdded: Date }],
               assignedTo: String,
               description: String,
               status: Number }],
    priorities: [{ID: Number, name: String}],
    ticketStatuses: [{ID: Number, name: String}],
    sprints: [{ID: Number, name: String, startDate: Date, endDate: Date}]
});

module.exports = mongoose.model('Project', projectSchema);