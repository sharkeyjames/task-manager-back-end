const mongoose = require('mongoose');

const filterSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    project_id: mongoose.Schema.Types.ObjectId,
    user: String,
    tabNumber: Number,
    sprint: Number,
    priorities: [Number],
    statuses: [Number],
    assignedTo: [String]
});

module.exports = mongoose.model('Filter', filterSchema);