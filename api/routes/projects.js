const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Project = require("../models/project");

//Returns all Projects
//Testing Method
router.get("/", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  // if(!req.session.user.manager){
  //   return res.status(401).send();
  // }
  Project.find()
    .exec()
    .then(docs => {
      console.log(docs);
      return res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      return res.status(500).json({
        error: err
      });
    });
});

//Returns all projects that a given user is admin for
//Must be projects for logged in user
router.get("/myProjects/:username", (req, res, next) => {
  // if(!req.session.user){
  //   console.log(req.session.user)
  //   return res.status(401).send();
  // }
  // if(!req.session.user.username != req.params.username){
  //   console.log(req.session.user.username)
  //   return res.status(401).send();
  // }
  const username = req.params.username;
  var conditions = { admins: username };
  Project.find(conditions)
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Returns all projects that a given user collaberates on
//Must be projects for logged in user
router.get("/collabProjects/:username", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  // if(!req.session.user.username != req.params.username){
  //   return res.status(401).send();
  // }
  const username = req.params.username;
  var conditions = { users: username };
  Project.find(conditions)
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Returns project for given _id
//Testing Method
router.get("/:projectId", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  const id = req.params.projectId;
  Project.findOne({ _id: id })
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res
          .status(404)
          .json({ message: "Project not found" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

//Deletes project for given _id
//Testing Method
router.delete("/:projectId", (req, res, next) => {
  const id = req.params.projectId;
  Project.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Creates a new project
router.post("/new", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  const project = new Project({
    _id: new mongoose.Types.ObjectId(),
    title: req.body.title,
    admins: req.body.admins,
    users: req.body.users,
    tickets: req.body.tickets,
    priorities: req.body.priorities,
    ticketStatuses: req.body.ticketStatuses,
    sprints: req.body.sprints
  });
  project
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Handling POST requests to /projects/new",
        createdProject: result
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Edits a project
//Should check that the current user is admin
router.post("/edit", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  console.log(req.body._id);
  console.log(req.body.tickets);
  Project.findOne({ _id: req.body._id }, function (err, project) {
      project.title = req.body.title,
      project.admins = req.body.admins,
      project.users = req.body.users,
      project.tickets = req.body.tickets,
      project.priorities = req.body.priorities,
      project.ticketStatuses = req.body.ticketStatuses,
      project.sprints = req.body.sprints
      project
      .save()
      .then(result => {
        console.log(result);
        res.status(201).json({
          message: "Handling POST requests to /projects/edit",
          createdProject: result
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });
});

module.exports = router;
