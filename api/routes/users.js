const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const User = require("../models/user");

const numberOfRoundsForSaltGeneration = 10;

//Returns all users
//Testing Method
router.get("/", (req, res, next) => {
  User.find()
    .exec()
    .then(docs => {
      console.log(docs);
      res.status(200).json(docs);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Creates a new user
//No requirements
router.post("/", (req, res, next) => {
  User.count({username: req.body.username}, function(error, count){
    if (count > 0) {
      return res.status(400).json({
        error: "User already exists"
      });
    }
    else{
      var hash = bcrypt.hashSync(req.body.password, numberOfRoundsForSaltGeneration);
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        username: req.body.username,
        passwordHash: hash
      });
      user
        .save()
        .then(result => {
          console.log(result);
          req.session.user = result;
          res.status(201).json(result);
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
    }
  })
});

//Creates a new manager
//No requirements
router.post("/manager", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  // if(!req.session.user.manager && req.session.user.username != 'manager'){
  //   return res.status(401).send();
  // }
  var hash = bcrypt.hashSync(req.body.password, numberOfRoundsForSaltGeneration);
  const user = new User({
    _id: new mongoose.Types.ObjectId(),
    username: req.body.username,
    passwordHash: hash,
    manager: true
  });
  user
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

//Changes a user's password
//Must be logged in as the user
//Must know the users current password
router.post("/changePassword", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  // if(!req.session.user.username != req.body.username){
  //   return res.status(401).send();
  // }
  const usernameParam = req.body.username;

  User.findOne({ username: usernameParam }, function (err, user) {
    console.log("From database", user);
    if (user) {
      console.log(user.passwordHash)

      if (bcrypt.compareSync(req.body.currentPassword, user.passwordHash)) {
        var hash = bcrypt.hashSync(req.body.newPassword, numberOfRoundsForSaltGeneration);
        user.passwordHash = hash;
        user.save(function (err) {
          if (err) {
            res.status(500).json({ error: err });
          }
          res.status(200).json(user);
        })
      } else {
        res.status(500).json({ error: "Current password incorrect" });
      }
    } else {
      res.status(500).json({ error: "User not found" });
    }
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

//Logout
//No Requirements
router.get("/logout", (req, res, next) => {
  res.status(200).send();
});

//Get public information about a user
//No requirements
router.get("/getMyUser", (req, res, next) => {
  // if(!req.session.user){
  //   return res.status(401).send();
  // }
  const usernameParam = req.session.user.username;
  User.findOne({ username: usernameParam }, { passwordHash: 0 })
    .exec()
    .then(doc => {

      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res
          .status(404)
          .json({ message: "User not found" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

//Get public information about a user
//No requirements
router.get("/:username", (req, res, next) => {
  const usernameParam = req.body.username;
  User.findOne({ username: usernameParam }, { passwordHash: 0 })
    .exec()
    .then(doc => {

      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res
          .status(404)
          .json({ message: "User not found" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

//Get public information about a user
//No requirements
router.post("/login", (req, res, next) => {
  const usernameParam = req.body.username;
  const passwordParam = req.body.password;
  User.findOne({ username: usernameParam })
    .exec()
    .then(user => {
      console.log("found User");
      console.log(user);
      if (bcrypt.compareSync(passwordParam, user.passwordHash)) {
        req.session.user = user;
        res.status(200).json(user);
      }
      else {
        res.status(500).json({ error: "Login Unsucessful" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: "Login Unsucessful" });
    });
});

//Delete a user
//Testing Method
router.delete("/:userId", (req, res, next) => {
  const id = req.params.userId;
  User.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
